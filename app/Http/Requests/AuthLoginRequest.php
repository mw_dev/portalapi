<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthLoginRequest extends FormRequest
{

    /**
     * Gets the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => [
                'required',
                'string',
                'min:2',
                'max:60',
                'regex:/^[a-z0-9_\-@\.]*$/i',
            ],
            'password' => [
                'required',
                'string',
                'min:4',
                'max:100',
            ]
        ];
    }

}
