<?php

namespace App\Http\Middleware;

use App\Services\UsersService;
use Closure;

class User
{

    protected $userService;

    public function __construct(UsersService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        if (!$request->header('uuid') && !$user = $this->userService->one($request->headers->get('uuid'))) {
            throw new \Exception(401);
        }

        return $next($request);
    }
}
