<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;


class WhiteList
{

    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next)
    {
        $list = explode(',', config('portal.white_list'));

        if (!in_array($request->ip(), $list)) {
            throw new \Exception(401);
        }

        return $next($request);

    }

}
