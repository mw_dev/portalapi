<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;


class CheckToken
{

    /**
     * @param Request $request
     * @param Closure $next
     * @param null $guard
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        $token = $request->headers->get('access_token');

        if (!$token || ($token !== config('portal.access_token'))) {
            throw new \Exception(401);
        }

        return $next($request);

    }
}
