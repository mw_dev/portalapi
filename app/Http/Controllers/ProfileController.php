<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateEmailRequest;
use App\Http\Resources\ProfileResource;
use App\Services\UsersService;

use Illuminate\Routing\Controller as BaseController;

class ProfileController extends BaseController
{
    protected $usersService;

    public function __construct(
        UsersService $usersService
    )
    {
        $this->usersService = $usersService;
    }


    /**
     * @param UpdateEmailRequest $request
     * @return ProfileResource
     */
    public function update(UpdateEmailRequest $request)
    {
        $id = $request->header('uuid');
        $email = $request->input('email');

        if (!$saved = $this->usersService->update($id, $email)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return ProfileResource::make($saved);
    }

}
