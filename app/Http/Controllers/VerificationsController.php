<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckSecretKeyRequest;
use App\Http\Requests\EmailVerificationRequest;
use App\Http\Resources\VerificationResource;
use App\Mail\SecretKey;
use App\Models\Verification;
use App\Services\UsersService;
use App\Services\VerificationsService;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\HttpFoundation\Response;

class VerificationsController extends BaseController
{

    protected $usersService;
    protected $verificationsService;

    public function __construct(
        UsersService $usersService,
        VerificationsService $verificationsService
    )
    {
        $this->usersService = $usersService;
        $this->verificationsService = $verificationsService;
    }

    /**
     * @param EmailVerificationRequest $request
     * @return bool
     * @throws \Exception
     */
    public function verify(EmailVerificationRequest $request)
    {
        $email = $request->input('email');
        if (!$user = $this->usersService->byEmail($email)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'User not found');
        }

        if ($oldToken = $this->verificationsService->findOneByUserId($user->id)) {
            $this->verificationsService->delete($oldToken->id);
        }

        if (!$token = $this->verificationsService->createVerifyToken($user->id)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        Mail::to($user)->send(new SecretKey($token));

        return true;
    }


    /**
     * @param CheckSecretKeyRequest $request
     * @return VerificationResource
     * @throws \Exception
     */
    public function check(CheckSecretKeyRequest $request)
    {
        $secretKey = $request->get('secretKey');

        if (!$verification = $this->verificationsService->findOneByToken($secretKey)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'User not found');
        }

        /** @var Verification $verification */
        if ($this->verificationsService->isExpired($verification)) {

            $this->verificationsService->delete($verification->id);
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'Token is expired');
        }

        $this->verificationsService->delete($verification->id);

        return VerificationResource::make($verification);
    }

}
