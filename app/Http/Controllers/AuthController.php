<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthLoginRequest;
use App\Http\Resources\ProfileResource;
use App\Services\UsersService;
use App\Services\VerificationsService;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class AuthController extends BaseController
{

    protected $usersService;
    protected $verificationsService;

    public function __construct(
        UsersService $usersService,
        VerificationsService $verificationsService
    )
    {
        $this->usersService = $usersService;
        $this->verificationsService = $verificationsService;
    }

    /**
     * @param AuthLoginRequest $request
     * @return array
     */
    public function login(AuthLoginRequest $request)
    {

        $login = $request->input('login');
        $password = $request->input('password');
        $user = $this->usersService->byLogin($login);

        if (!$this->usersService->validatePassword($login, $password)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'Incorrect login or password');
        }

        if (!$token = $this->usersService->updateToken($user->id)) {
            abort(Response::HTTP_UNPROCESSABLE_ENTITY, 'Incorrect login or password');
        }

        return [
            'token' => $token,
            'user' => ProfileResource::make($user)
        ];

    }
}
