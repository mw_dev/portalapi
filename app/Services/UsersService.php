<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class UsersService
 *
 * @package App\Services
 */
class UsersService
{

    /**
     * @param string $login
     * @param string $password
     * @return bool
     */
    public function validatePassword(string $login, string $password): bool
    {
        if ($user = $this->byLogin($login)) {
            return app('hash')->check($password, $user->password);
        }

        return false;
    }

    /**
     * @param string $login
     * @return Builder|Model|object|null
     */
    public function byLogin(string $login)
    {
        return User::query()
            ->where(['name' => $login])
            ->first();
    }


    /**
     * @param string $email
     * @return Builder|Model|object|null
     */
    public function byEmail(string $email)
    {
        return User::query()
            ->where(['email' => $email])
            ->first();
    }

    /**
     * @param string $id
     * @param array $with
     * @return Builder|Model|object|null
     */
    public function one(string $id, array $with = [])
    {
        return User::query()
            ->with($with)
            ->where(['id' => $id])
            ->first();
    }

    /**
     * @param string $id
     * @return bool|string
     */
    public function updateToken(string $id)
    {
        if (!$user = $this->one($id)) {
            return false;
        }

        $token = Str::random(60);

        return $user->forceFill([
            'api_token' => hash('sha256', $token),
        ])->save() ? $token : false;
    }

    /**
     * @param string $id
     * @param string $email
     * @return bool|Model
     */
    public function update(string $id, string $email)
    {
        $user = $this->one($id);
        $user->fill([
            'email' => $email
        ]);

        if (!$user->save()) {
            return false;
        }

        return $user->refresh();
    }
}
