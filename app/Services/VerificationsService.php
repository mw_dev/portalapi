<?php

namespace App\Services;

use App\Models\Verification;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;

/**
 * Class VerificationsService
 *
 * @package App\Services
 */
class VerificationsService
{

    /**
     * @param string $id
     * @param array $with
     * @return Builder|Model|object|null
     */
    public function one(string $id, array $with = [])
    {
        return Verification::query()
            ->with($with)
            ->where(['id' => $id])
            ->first();
    }

    /**
     * @param Verification $verification
     * @return bool
     * @throws Exception
     */
    public function isExpired(Verification $verification): bool
    {
        $now = (new \DateTime('now'))->getTimestamp();

        $created_at = (new \DateTime($verification->created_at))
            ->modify('+' . $verification->expire . ' minutes')
            ->getTimestamp();

        return $now > $created_at;
    }

    /**
     * @param string $id
     * @return string
     */
    public function createVerifyToken(string $id): string
    {

        $expiresIn = config('portal.expires_in');
        $token = $this->makeToken();

        return Verification::make([
            'token' => $token,
            'expire' => $expiresIn,
            'user_id' => $id,
        ])->save() ? $token : '';

    }

    /**
     * @param string $token
     * @return Builder|Model|object|null
     */
    public function findOneByToken(string $token)
    {
        return Verification::query()
            ->where(['token' => $token])
            ->first();
    }

    /**
     * @param string $id
     * @return Builder|Model|object|null
     */
    public function findOneByUserId(string $id)
    {
        return Verification::query()
            ->where(['user_id' => $id])
            ->first();
    }

    /**
     * @return string
     */
    public function makeToken(): string
    {
        return Str::random(32);
    }

    /**
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function delete(int $id): bool
    {
        if (!$item = $this->one($id)) {
            return false;
        }

        return $item->delete();
    }
}
