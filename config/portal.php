<?php

return [
    'web_url' => env('WEB_URL', 'localhost'),
    'access_token' => env('ACCESS_TOKEN', ''),
    'white_list' => env('WHITE_LIST', ''),
    'expires_in' => env('EXPIRES_IN_MINUTES', 120)
];
