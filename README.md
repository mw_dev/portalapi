# Getting started

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/master/installation)

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate:refresh --seed

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    composer install
    cp .env.example .env
    php artisan key:generate
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate:refresh --seed
    php artisan serve


## Environment variables

- `.env` - Environment variables that should be set in this file

    DB_DATABASE=

    DB_USERNAME=

    DB_PASSWORD=

    MAIL_MAILER=smtp

    MAIL_HOST=smtp.postmarkapp.com

    MAIL_PORT=587

    MAIL_USERNAME=postmark server token
    
    MAIL_PASSWORD=postmark server token

    MAIL_FROM_ADDRESS=postmark senders email address
    
    MAIL_FROM_NAME=postmark senders from name

    POSTMARK_TOKEN=postmark server token
    
    ACCESS_TOKEN=generate any token -  the same token as you create in .env file in portal  

    WEB_URL=portal url

    WHITE_LIST=contains a list of available IPs separated by commas and must not contain spaces

    EXPIRES_IN_MINUTES=token expiration time in minutes

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.
