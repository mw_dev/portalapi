<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\VerificationsController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(
    [
        'prefix' => 'v1',
    ],
    static function () {
        Route::group(
            [
                'prefix' => 'auth'
            ],
            static function () {
                Route::post('', [VerificationsController::class, 'verify']);
                Route::post('check', [VerificationsController::class, 'check']);
            }
        );

        Route::group(
            [
                'prefix' => 'profile'
            ],
            static function () {
                Route::put('update', [ProfileController::class, 'update'])
                    ->middleware('uuid');
            }
        );
    }
);

Route::any('{all}', static function () {
    abort(404);
})->where('all', '.*');
